# Ivegotaroom.com.au

## Aim

Ivegotaroom.com.au is a website for helping people who need help match with people who can give it, in light of the Australian fires and floods.
Users can register their needs or haves on a large map, and find people in a nearby radius who can provide needed assistance.
People can message directly through the website and manage details of themselves and their registered needs/haves through their profile.

## Background

Ivegotaroom.com.au is developed in NodeJS and React, with a MongoDB database. The Google Maps JS API is used for the map capabilities.

## Progress

Ivegotaroom is in active development and will be deployed in the near future.