const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const pino = require('express-pino-logger')();
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const sharedsession = require("express-socket.io-session");
const socketIo = require("socket.io");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

// bcrypt salting

const saltRounds = 10;

// Connect to Mongo

mongoose.connect('mongodb://localhost/ivegotaroom', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

// MongoDB Schemas

const ItemSchema = new Schema ({
  needOrHave: Boolean,
  lat: Number,
  lng: Number,
  type: Number,
  dateFrom: Date,
  dateTo: Date,
  quantity: String,
  subtype: String,
  notes: String,
  radius: Number
});

const MessageSchema = new Schema ({
  to: ObjectId,
  from: ObjectId,
  timeSent: Date,
  content: String,
  read: Boolean
});

const UserSchema = new Schema({
  password: String,
  email: String,
  phone: String,
  firstname: String,
  surname: String,
  messages: [ObjectId],
  places: [ItemSchema]
});

// MongoDB Models

//const Item = mongoose.model('Item', ItemSchema);
const Message = mongoose.model('Message', MessageSchema);
const User = mongoose.model('User', UserSchema);

// App use setup

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(pino);
app.use(cookieParser());

// Session setup

const sessionInstance = session({
  key: 'user_sid',
  secret: 'hellogood damnableteddy fudgeandrollover',
  resave: false,
  saveUninitialized: false,
  cookie: {
      expires: 600000
  }
})

app.use(sessionInstance);

// Clear cookies on dead session

app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
      res.clearCookie('user_sid');        
  }
  next();
});

// Session checker for logged-in redirecting

/*
var sessionChecker = (req, res, next) => {
  if (req.session.user && req.cookies.user_sid) {
      res.redirect('/dashboard');
  } else {
      next();
  }    
};

app.get('/', sessionChecker, (req, res) => {
    res.redirect('/login');
});
*/

// Routes

app.post('/register/user', (req, res) => {
  const formValues = req.body.formValues;
  res.setHeader('Content-Type', 'application/json');
  bcrypt.hash(formValues.password, saltRounds, (err, hash) => {
    if (err) {
      console.log(err);
      res.send({err: err});
      return;
    }
    const newUser = new User ({
      firstname: formValues.firstname,
      surname: formValues.surname,
      password: hash,
      email: formValues.email,
      phone: formValues.phone
    });
    newUser.save(err => {
      if (err) {
        console.log(err);
        res.send({err: err});
      } else {
        req.session.user = {
          firstname: newUser.firstname,
          surname: newUser.surname
        };
        console.log("Stored new user!");
        res.send({success: true});
      }
    });
  });
});

app.get('/checksession', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  if (req.session.user && req.cookies.user_sid) {
    res.send({user: req.session.user});
  } else {
    res.send({});
  }
});

app.get('/places', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  User.find({}, "_id firstname surname email phone places", (err, users) => {

    if (err) {
      res.send({err:err});
    } else {
      res.send({users: users});
    }
  });
});

app.post('/login', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const email = req.body.email;
  User.findOne({email: email}, (err, user) => {
    if (err) {
      console.log(err);
      res.send({err: err});
      return;
    } else if (!user) {
      res.send({loggedin:false});
      return;
    }

    bcrypt.compare(req.body.password, user.password, (err, valid) => {
      if (err) {
        console.log(err);
        res.send({err: err});
        return;
      }
      if (valid) {
        req.session.user = {
          firstname: user.firstname,
          surname: user.surname,
          id: user._id
        };
        
        res.send({loggedin: true});
      } else {
        res.send({loggedin: false})
      }
    });
  })
});

app.post('/logout', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  if (req.session.user && req.cookies.user_sid) {
    res.clearCookie('user_sid');
    res.send({loggedout: true});
  } else {
    res.send({loggedout: false});
  }
});

app.post('/register/place', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  
  const formValues = req.body.formValues;
  const newItem = {
    needOrHave: formValues.needOrHave,
    type: formValues.type,
    dateFrom: formValues.dateFrom,
    dateTo: formValues.dateTo,
    quantity: formValues.quantity,
    subtype: formValues.subtype,
    notes: formValues.notes,
    lat: formValues.lat,
    lng: formValues.lng,
    radius: formValues.radius,
  };

  console.log("Adding new place: ");
  console.log(formValues);


  User.updateOne({_id: mongoose.Types.ObjectId(formValues.user)}, {
    $push: {
      places: newItem
    }
  }, (err) => {
    if (err) {
      console.log(err);
      res.send({err: err});
    } else {
      console.log("Stored new place!");
      res.send({success: true});
    }
  });
});

app.get("/messages", (req,res) => {
  res.setHeader('Content-Type', 'application/json');
  if (req.session.user && req.cookies.user_sid) {
    let msgRes = {};
    if (!req.query.with) {
      Message.find({from: req.session.user.id}, (err, fromMessages) => {
        if (err) {
          console.log(err);
          res.send({err: err});
        } else {
          msgRes.from = fromMessages;
          Message.find({to: req.session.user.id}, (err, toMessages) => {
            if (err) {
              console.log(err);
              res.send({err: err});
            } else {
              msgRes.to = toMessages;
              res.send(msgRes);
            }
          });
        }
      });
    } else {
      Message.find({from: req.session.user.id, to:req.query.with}, (err, fromMessages) => {
        if (err) {
          console.log(err);
          res.send({err: err});
        } else {
          msgRes.messages = fromMessages;
          Message.find({to: req.session.user.id, from:req.query.with}, (err, toMessages) => {
            if (err) {
              console.log(err);
              res.send({err: err});
            } else {
              msgRes.messages = msgRes.messages.concat(toMessages);
              msgRes.messages = msgRes.messages.sort((a, b) => b.timeSent - a.timeSent);
              res.send(msgRes);
            }
          });
        }
      });
    }
  } else {
    res.send({err: "Not logged in!"});
  }
});

// Catch all other requests
/*
app.get('*', (req,res) =>{
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});
*/

// Server and Socket IO setup

const server = http.createServer(app);

const io = socketIo(server);

// Shared Session setup

io.use(sharedsession(sessionInstance, {
  autoSave:true
}));

// Socket IO logic

io.on("connection", socket => {
  console.log("New client connected");
  /*setInterval(
    () => {
      socket.emit("message", "HI!! Oh my god have you seen Harry? What a magnificent person!");
    },
    10000
  );*/

  socket.on('message sent', function (msg) {
    console.log('I received a private message saying ', msg);
    const date = new Date();
    const today = date.toISOString();
    const newMessage = new Message({
      from: msg.from,
      to: msg.to,
      content: msg.content,
      timeSent: today,
      read: false
    });
    newMessage.save(err => {
      if (err) {
        console.log(err);
        socket.emit('message failed', msg);
      } else {
        console.log("Message saved!");
        socket.emit('message update', msg);
      }
    });
  });

  socket.on("get messages", function(other) {
    console.log("Got message request...");
    let msgRes = {};
    const user = socket.handshake.session.user;
    console.log(user);
    if (user) {
      const fromTime = other.timeFrom;
      console.log(fromTime);
      Message.find({from: user.id, to:other.other, timeSent: { $lt: fromTime }}).sort({timeSent: -1}).limit(other.number).exec((err, fromMessages) => {
        if (err) {
          console.log(err);
          socket.emit("error", err)
        } else {
          console.log(fromMessages);
          msgRes.messages = fromMessages;
          Message.find({to: user.id, from:other.other, timeSent: { $lt: fromTime }}).sort({timeSent: -1}).limit(other.number).exec((err, toMessages) => {
            if (err) {
              console.log(err);
              socket.emit("error", err)
            } else {
              msgRes.messages = msgRes.messages.concat(toMessages);
              msgRes.messages = msgRes.messages.sort((a, b) => a.timeSent - b.timeSent);
              socket.emit("message load", msgRes.messages);
              if (other.init) {
                socket.emit("scroll");
              }
            }
          });
        }
      });
    } else {
      socket.emit("error", "Not logged in!");
    }
  });

  socket.on("mark read", function(data) {
    const user = socket.handshake.session.user;
    if (user) {
      Message.updateMany({to: user.id, from: data.other}, {read: true}, err => {
        if (err) {
          console.log(err);
          socket.emit("error", err);
          return;
        }
        socket.emit("read");
      });
    } else {
      socket.emit("error", "Not logged in!");
    }
  });

  socket.on("get pending", function(data) {
    const user = socket.handshake.session.user;
    if (user) {
      Message.find({to: user.id, read: false}).select("from").populate("from").exec().then((pending) => {
        let idCount = [];
        pending.forEach(id => {
          let idExists = false;
          idCount.forEach(idObj => {
            if (idObj.id === id.from._id) {
              idExists = true;
              idObj.count ++;
              return;
            }
          });
          console.log(id);
          if (!idExists) {
            idCount.push({id: id.from._id, name: from.firstname + " " + from.surname, count: 1});
          }
        });
        socket.emit("pending", idCount);
      }).catch(err => {
        console.log(err);
        socket.emit("error", err);
        return;
      });
    } else {
      socket.emit("error", "Not logged in!");
    }
  });

  socket.on("get contacts", async data => {
    const user = socket.handshake.session.user;
    if (user) {
      Message.find({to: user.id}, "from", (err, msgsToUser) => {
        if (err) {
          console.log(err);
          socket.emit("error", err);
          return;
        }
        Message.find({from: user.id}, "to", (err, msgsFromUser) => {
          if (err) {
            console.log(err);
            socket.emit("error", err);
            return;
          }
          let userIds = [];
          msgsToUser.forEach(msgObj => {
            const from = msgObj.from.toString();
            if (!userIds.includes(from)) {
              userIds.push(from);
            }
          });
          msgsFromUser.forEach(msgObj => {
            const to = msgObj.to.toString();
            if (!userIds.includes(to)) {
              userIds.push(to);
            }
          });
          console.log("User IDs: ");
          let users = [];
          let counter = 0;
          userIds.forEach(async userId => {
            counter++;
            const userFunc = function () {
              return User.findById(userId, "firstname surname").exec().then(user => {
                return user;
              }).catch(err => {
                console.log(err);
                socket.emit("error", err);
                return;
              });
            }
            const user = await userFunc();
            console.log(user);
            users.push(user.firstname + " " + user.surname);
            if (counter === userIds.length) {
              console.log(users);
              socket.emit("contacts", users);
              return;
            }
          });
        });
      });
    } else {
      socket.emit("error", "Not logged in!");
    }
  });

  socket.on("disconnect", () => console.log("Client disconnected"));
});

// Server activation

server.listen(3001, () =>
  console.log('Express server is running on localhost:3001')
);