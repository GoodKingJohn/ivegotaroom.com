import React, { Component, Fragment } from 'react';
import socketIOClient from "socket.io-client";
import { withRouter } from "react-router-dom";
import './Chatbox.css';

class Chatbox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            minimised: false,
            chat: [],
            loadedAll: false,
            hasUnread: false
        };

        this.inputIsFocused = false;

        this.contentRef = React.createRef();
        this.lastMsgRef = React.createRef();

        this.socket = null;
        this.minimise = this.minimise.bind(this);
        this.inputRef = React.createRef();
        this.handleInputKeyDown = this.handleInputKeyDown.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
        this.loadChat = this.loadChat.bind(this);
    }

    componentDidMount () {
        this.socket = socketIOClient('http://localhost:3001');
        this.socket.on("message update", data => {
            let unread = false;
            if (this.state.minimised) {
                unread = true;
            } else {
                if (data.from === this.props.to.id)
                    this.markRead();
            }
            this.setState({ hasUnread: unread, chat: this.state.chat.concat([{fromMe: data.from === this.props.user.id, content: data.content, timeSent: data.timeSent, read: data.read}])});
        });
        this.socket.on("message failed", data => {
            this.setState({ chat: this.state.chat.concat([{failed: true, fromMe: data.from === this.props.user.id, content: data.content, timeSent: data.timeSent, read: data.read}])});
            this.scroll();
        });
        this.socket.on("message load", data => {
            const messages = data.map((message) => ({fromMe: message.from === this.props.user.id, content: message.content, timeSent: message.timeSent, read: message.read}));
            const loadedAll = messages.length === 0;
            let unread = false;
            messages.forEach(message => {
                if (!message.read && !message.fromMe) {
                    unread = true;
                    return;
                }
            });
            console.log("hasUnread: " + unread);
            const scrollTo = this.lastMsgRef.current ? this.lastMsgRef.current.offsetTop : 0;
            this.setState({ chat: messages.concat(this.state.chat), loadedAll: loadedAll, hasUnread: unread});
            if (this.lastMsgRef.current) {
                this.contentRef.current.scrollTop = scrollTo;
            }
        });
        this.socket.on("scroll", () => {
            this.scroll();
        });
        this.socket.on("read", () => {
            console.log("Messages are read");
            let messages = this.state.chat.slice();
            messages.forEach(message => {
                message.read = true;
            });
            this.setState({chat: messages, hasUnread: false});
        });
        this.contentRef.current.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount () {
        if (this.contentRef.current) {
            this.contentRef.current.removeEventListener('scroll', this.handleScroll);
        }
    }

    updateChat () {
        this.setState({chat: []}, () => {
            const date = new Date();
            const today = date.toISOString();
            this.socket.emit("get messages", {user: this.props.user.id, other: this.props.to.id, number: 10, timeFrom: today, init: true});
        });
    }

    loadChat () {
        const timeFrom = this.state.chat[0].timeSent;
        this.socket.emit("get messages", {user: this.props.user.id, other: this.props.to.id, number: 10, timeFrom: timeFrom});
    }

    markRead () {
        console.log("Client marking read");
        this.socket.emit("mark read", {other: this.props.to.id});
    }

    open () {
        this.setState({minimised: false});
    }

    scroll () {
        this.contentRef.current.scrollTop = this.contentRef.current.scrollHeight;
    }

    render () {
        console.log("Rendering unread? " + this.state.hasUnread);
        return (
            <div className="chatContainer">
                <div onClick={this.minimise} className={"clickable" + (this.state.hasUnread ? " unread" : "")}>
                    <b>CHAT</b><br/>
                    <p>to {this.props.to.firstname} {this.props.to.surname}</p>
                </div>
                <hr/>
                {!this.state.minimised &&
                    <div className="chatBox">
                        <div className="chatContent" ref={this.contentRef}>
                            <div className="messages">
                                <table className="fw">
                                    <tbody className="fw">
                                        <tr className="fw">
                                            <td className={"loadBtn" + (this.state.loadedAll ? " loadBtnDisable" : "")} onClick={this.loadChat}>
                                                {this.state.loadedAll ? "Beggining of chat" : "Load more messages"}
                                            </td>
                                        </tr>
                                        {this.state.chat.map((item, index) => (
                                            index === 0 ?
                                            <tr key={index} className="fw" ref={this.lastMsgRef}>
                                                <td className={(item.fromMe ? "me" : "you") + " msg" + (item.failed ? " fail" : "")}>
                                                    <p>{item.content}</p>
                                                    {item.failed && <p className="error">! Failed to send</p>}
                                                </td>
                                            </tr>
                                            :
                                            <tr key={index} className="fw">
                                                <td className={(item.fromMe ? "me" : "you") + " msg" + (item.failed ? " fail" : "")}>
                                                    <p>{item.content}</p>
                                                    {item.failed && <p className="error">! Failed to send</p>}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <input type="text" className="messageBox" onKeyDown={this.handleInputKeyDown} ref={this.inputRef}/>
                    </div>
                }
            </div>
        )
    }

    minimise () {
        if (this.props.user) {
            if (this.state.minimised) {
                this.markRead();
            }
            this.setState({minimised: !this.state.minimised});
        } else {
            //this.props.history.push("/register");
        }
    }

    handleInputKeyDown(e) {
        if (e.key === 'Enter') {
            //this.setState({chat: this.state.chat.concat([{fromMe: true, content: this.inputRef.current.value}])});
            //console.log("Message from " + this.props.user.id + " to " + this.props.to.id)
            this.socket.emit("message sent", {from: this.props.user.id, to: this.props.to.id, content: this.inputRef.current.value});
            this.inputRef.current.value = "";
            this.scroll(true);
        }
    }

    handleScroll () {
        if (this.contentRef.current.scrollTop == 0) {
            this.loadChat ();
        }
    }

}

export default Chatbox;