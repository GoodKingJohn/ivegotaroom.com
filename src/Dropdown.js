import React, {Component} from 'react';

class Dropdown extends Component {

    constructor (props) {
        super(props);
        const useDefault = (this.props.default && this.props.default) || (this.props.default === 0);
        this.state = {
            selected: useDefault ? this.props.default : -1,
            clicked: false,
            item: useDefault ? this.props.options[this.props.default] : null
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    render () {
        return (
            <div className="dropdownContainer" onMouseLeave={this.handleMouseLeave}>
                <div className="dropdown" onClick={this.handleClick}>
                    <p>{this.state.selected === -1 ? "Select..." : this.state.item}</p>
                </div>
                <div className="ddOptions" style={{visibility: this.state.clicked ? "visible" : "hidden"}}>
                    {this.props.options.map((item, i) => {
                         return <div className="ddOption" key={i} onClick={() => {this.handleSelect(i)}}>{item}</div>;
                    })}
                </div>
            </div>
        );
    }

    handleClick () {
        this.setState({clicked: !this.state.clicked});
    }

    handleSelect (index) {
        this.setState({selected: index, item: this.props.options[index], clicked:false});
        if (this.props.onSelect) {
            this.props.onSelect(index, this.props.options[index]);
        }
    }

    handleMouseLeave () {
        this.setState({clicked: false});
    }

}

export default Dropdown;