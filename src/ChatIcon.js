import React, { Component, Fragment } from 'react';
import socketIOClient from "socket.io-client";
import { withRouter } from "react-router-dom";
import './ChatIcon.css';

class ChatIcon extends Component {

    constructor (props) {
        super (props);
        this.state = {
            count: 0
        }
        this.onIconClick = this.onIconClick.bind(this);
    }

    componentDidMount () {
        this.socket = socketIOClient('http://localhost:3001');
        this.socket.on("pending", data => {
            console.log(data);
            this.setState({count: data.length});
        });
        this.socket.emit("get pending");
    }

    render () {
        return (
            <div className="chatIcon" onClick={this.onIconClick}>
                <i className="fas fa-comment-alt"></i>
                {this.state.count > 0 && <div className="notif">{this.state.count}</div>}
            </div>
        );
    }

    onIconClick () {
        this.props.history.push("/profile#chat");
    }

}

export default withRouter(ChatIcon);