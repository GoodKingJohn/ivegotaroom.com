import React, { Component, Fragment } from 'react';
// examples:
import GoogleMap from 'google-map-react';

// consts

const InfoWindow = (props) => {
  const { place } = props;
  const infoWindowStyle = {
    position: 'relative',
    bottom: 150,
    left: '-45px',
    width: 220,
    backgroundColor: 'white',
    boxShadow: '0 2px 7px 1px rgba(0, 0, 0, 0.3)',
    padding: 10,
    fontSize: 14,
    zIndex: 100,
  };

  return (
    <div style={infoWindowStyle}>
      <div style={{ fontSize: 16 }}>
        {place.name}
      </div>
      <div style={{ fontSize: 14 }}>
        <span style={{ color: 'grey' }}>
          {place.rating}{' '}
        </span>
        <span style={{ color: 'orange' }}>
          {String.fromCharCode(9733).repeat(Math.floor(place.rating))}
        </span>
        <span style={{ color: 'lightgrey' }}>
          {String.fromCharCode(9733).repeat(5 - Math.floor(place.rating))}
        </span>
      </div>
      <div style={{ fontSize: 14, color: 'grey' }}>
        {place.types[0]}
      </div>
      <div style={{ fontSize: 14, color: 'grey' }}>
        {'$'.repeat(place.price_level)}
      </div>
      <div style={{ fontSize: 14, color: 'green' }}>
        {place.opening_hours.open_now ? 'Open' : 'Closed'}
      </div>
    </div>
  );
};

// Marker component
const Marker = (props) => {

  const markerStyle = {
    border: '1px solid black',
    borderRadius: '50%',
    backgroundColor: 'white',
    padding: 2,
    margin: 'auto',
    display: 'inline-block',
    color: props.place.type === 'need' ? 'red' : 'green',
    cursor: 'pointer',
    zIndex: 10,
    fontSize: 20,
  };

  return (
    <Fragment>
      <div style={markerStyle}><i className="fas fa-home"></i></div>
      {props.show && <InfoWindow place={props.place} />}
    </Fragment>
  );
};

class SelectMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mapApiLoaded: false,
      mapInstance: null,
      mapApi: null,
      startLoc: {},
      places: [],
    };
  }

  // TODO: Move circle rendering code to new Map component specifically for marking a Have
  // OR: modify this component to have markers enabled/disabled

  apiHasLoaded = (map, maps) => {
    this.setState({
      mapApiLoaded: true,
      mapInstance: map,
      mapApi: maps,
    });
    const circle = new maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: map,
      center: this.state.startLoc,
      radius: 1000000,
      editable: false,
      draggable: false,
      geodesic: true
    });
    maps.event.addListener(circle, 'mouseover', function () {
      circle.set('editable', true);
    });

    maps.event.addListener(circle, 'mouseout', function () {
      circle.set('editable', false);
    }); 
    circle.setMap(map);
  };

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    let startLoc = {
      lat: parseFloat(urlParams.get('lat')),
      lng: parseFloat(urlParams.get('lng'))
    }
    
    if (!startLoc.lat || !startLoc.lng) {
      startLoc = this.props.defaultCenter;
    }
    fetch('http://localhost:3000/places.json')
      .then(response => response.json())
      .then((data) => {
        data.results.forEach((result) => {
          result.show = false; // eslint-disable-line no-param-reassign
          let type = Math.random();
          result.type = (type > 0.5 ? 'need' : 'give');
        });
        this.setState({ places: data.results, startLoc: startLoc });
      });
  }

  // onChildClick callback can take two arguments: key and childProps
  onChildClickCallback = (key) => {
    this.setState((state) => {
      const index = state.places.findIndex(e => e.id === key);
      state.places[index].show = !state.places[index].show; // eslint-disable-line no-param-reassign
      return { places: state.places };
    });
  };

  render() {
    const {
      places, mapApiLoaded, mapInstance, mapApi,
    } = this.state;
    /*const data = [];
    for (var i = 0; i < 100; i++) {
      data.push({
        id: i,
        lat: Math.floor(Math.random() * Math.floor(180)) - 90,
        lng: Math.floor(Math.random() * Math.floor(360)) - 180,
        weight: Math.floor(Math.random() * Math.floor(5)),
      });
    }*/
    const data = places.map(place => ({
      lat: place.geometry.location.lat,
      lng: place.geometry.location.lng,
      weight: Math.floor(Math.random() * Math.floor(5)),
    }));
    const heatmapData = {
      positions: data,
      options: {
        radius: 20,
        opacity: 0.5,
      },
    };

    const urlParams = new URLSearchParams(window.location.search);
    const disableMarkers = urlParams.get('nomarkers');

    return (
      <Fragment>
        {data.length > 0 && (
          <GoogleMap
            bootstrapURLKeys={{
                key: "AIzaSyCmpEGgQ3qZjSVAEmf0VUlPI2ypl5D0b20",
                libraries: ['visualization']
            }}
            defaultCenter={this.state.startLoc}
            defaultZoom={this.props.defaultZoom}
            heatmap={heatmapData}
            onChildClick={this.onChildClickCallback}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map, maps }) => this.apiHasLoaded(map, maps)}
          >
            {!disableMarkers ? places.map(place =>(
                <Marker
                  key={place.id}
                  lat={place.geometry.location.lat}
                  lng={place.geometry.location.lng}
                  show={place.show}
                  place={place}
                />)
            ) : ""}
          </GoogleMap>
        )}
      </Fragment>
    );
  }
}

export default SelectMap;