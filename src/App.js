import React, { Component, Fragment } from 'react';
import './App.css';
import HelpMap from './HelpMap';
import ScrollButton from './ScrollButton';
import { withRouter, Route, Switch, Link } from 'react-router-dom';
import Home from './pages/Home';
import RegisterHaveRoom from './pages/haves/RegisterHaveRoom';
import RegisterHaveAnimal from './pages/haves/RegisterHaveAnimal';
import RegisterHaveResources from './pages/haves/RegisterHaveResources';
import RegisterUser from './pages/RegisterUser';
import Profile from './pages/Profile';
import HaveMap from './pages/HaveMap';
import Chatbox from './Chatbox';
import ChatIcon from './ChatIcon';

class App extends Component {

  constructor (props) {
    super(props);
    this.state = {
      user: null,
      chatTo: null
    }
    this.spaceRef = React.createRef();
    this.homeRef = React.createRef();
    this.mapRef = React.createRef();
    this.mapDragTarget = React.createRef();
    this.chatRef = React.createRef();

    this.checkForUser = this.checkForUser.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.openChat = this.openChat.bind(this);
  }

  componentDidMount () {
    this.checkForUser();
  }

  checkForUser () {
    console.log("Checking for user login...");
    fetch('/checksession', {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      headers: {
        "Content-Type": "application/json"
        // 'Content-Type': 'application/x-www-form-urlencoded',
      }
    }).then((response) => {
      return response.json();
    })
    .then((res) => {
      if (res.user) {
        console.log(res.user);
        this.setState({user: res.user});
      } else {
        this.setState({user: null});
      }
    });
  }

  render() {
    
    /* const ChatboxFunc = React.forwardRef((props, ref) => (
      <React.Fragment>
        
      </React.Fragment>
    ));*/

    return (
      <div className="App">
        <Switch>
          <Route path='/map' component={NonComponent}/>
          <Route exact path='/' component={MapHiderAbs}/>
          <Route component={MapHiderFix}/>
        </Switch>
        <div className="App-header" ref={this.homeRef}>
          <Link to="/">
            <img src="/logo.png" className="logo"/>
          </Link>
        </div>
        <div className="infoBannerHeader"><b>
          <i style={{fontSize:"20px"}} className="fas fa-hands-helping"></i> Help a mate!
          <div className="userOptions">
            <Link to={this.state.user ? "/profile" : "/register"}>
            <div className="userDisplay"><i className="fas fa-user"></i> {this.state.user ? "Welcome " + this.state.user.firstname + "!": "Login"}</div>
            </Link>
            {this.state.user &&
              <Fragment>
                <div className="userDisplay" alt="Sign out" onClick={this.handleLogout}>
                  <i className="fas fa-sign-out-alt"></i>
                </div>
                <ChatIcon/>
              </Fragment>
            }
          </div>
        </b></div>
        {(this.state.chatTo && this.state.user) && <Chatbox user={this.state.user} to={this.state.chatTo} ref={this.chatRef}/>}
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/have/room' component={RegisterHaveRoom}/>
          <Route path='/have/animal' component={RegisterHaveAnimal}/>
          <Route path='/have/resource' component={RegisterHaveResources}/>
          <Route path='/profile' component={Profile}/>
          <Route path='/register' render={(props) => <RegisterUser onSubmit={this.checkForUser}/>}/>
          <Route path='/chat' render={(props) => <Chatbox user={this.state.user} to={this.state.chatTo} ref={this.chatRef}/>}/>
          <Route path='/map' render={(props) => <HaveMap ref={this.mapDragTarget} map ={this.mapRef}/>}/>
        </Switch>
        <div id="mapContainer">
          <HelpMap
            defaultCenter={{
              lat: -25.155139,
              lng: 134.249334
            }}
            defaultZoom={5}
            onCircleDrag={this.mapDragTarget}
            ref={this.mapRef}
            user={this.state.user}
            openChat={this.openChat}
          >
          </HelpMap>
        </div>
        <Switch>
          <Route exact path='/' render={(props) => <MapBottom spaceRef={this.spaceRef} homeRef={this.homeRef}/>}/>
          <Route component={NonComponent}/>
        </Switch>
      </div>
    );
  }

  openChat (user) {
    this.setState({chatTo: user}, () => {
      if (this.chatRef.current) {
        this.chatRef.current.updateChat();
        this.chatRef.current.open();
      }
    });
  }

  handleLogout () {
    fetch('/logout', {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      headers: {
        "Content-Type": "application/json"
        // 'Content-Type': 'application/x-www-form-urlencoded',
      }
    }).then(response => {
      this.checkForUser();
      this.props.history.push("/");
    });
  }
}

const MapBottom = (props) => {
  return (
    <React.Fragment>
      <div className="stayBottom">
        <ScrollButton id="moreInfo" hover="View the Map" target={props.spaceRef}><i className="fas fa-arrow-down"></i></ScrollButton>
      </div>
      <div className="mapSpace" ref={props.spaceRef}></div>
      <div className="banners">
        <ScrollButton id="moreInfo" hover="Back to Home" target={props.homeRef}><i className="fas fa-arrow-up"></i></ScrollButton><br />
        <div className="infoBannerHeader">
          <b>This is the Help Map</b><br/>
          Zoom in to see individual markers
        </div>
        <div className="infoBanner">
          This map contains information on all people currently in need or able to give help. Use it to see where help is needed most, or who is closest to give help to you.
        </div>
      </div>
    </React.Fragment>
  );
}

const NonComponent = (props) => {
  return (<div/>);
}

const MapHiderAbs = (props) => {
  return (<div id="mapHiderAbs"></div>);
}

const MapHiderFix = (props) => {
  return (<div id="mapHiderFix"></div>);
}

export default withRouter(App);
