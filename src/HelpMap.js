import React, { Component, Fragment } from 'react';
// examples:
import GoogleMap from 'google-map-react';

// consts

const mapZoomLevels = {
  showMarkers: 13,
  postSearch: 17
};

class InfoWindow extends Component {

  render() {
    const { place, user } = this.props;
    let type = "";
    switch (place.type) {
      case 0:
        type = "Room";
        break;
      case 1:
        type = "Animal Space";
        break;
      case 2:
        type = "Resource";
        break;
      default:
        type = "Error"
    }

    const messageable = user && (user.id != place.user.id);

    return (
      <div className="infoWindow">
        <div className="typeDisplay" style={{backgroundColor: place.needOrHave ? "red" : "green"}}>
          {place.needOrHave ? "NEED" : "HAVE"}<br/>
          {type}
        </div>
        <div>
          <div style={{ fontSize: 16 }}>
            {place.user.firstname} {place.user.surname}
          </div>
          <div style={{fontSize: 10}}>
            {place.user.email}<br/>
            {place.user.phone}
          </div>
        </div>
        <div className={"chatButton" + (messageable ? "" : " greyedOut")} onClick={messageable ? () => this.openChat(place.user) : () => {}}>
          <i className="fas fa-comment-alt"></i>
          {!user && <p style={{fontSize:8, color:"black"}}>Login to message</p>}
        </div>
      </div>
    );
  }

  openChat(user) {
    if (this.props.openChat) {
      console.log("Holy fucking shit ive been clicked what the actual fuck jesus christ");
      this.props.openChat(user);
    }
  }
}

// Marker component
const Marker = (props) => {

  const markerStyle = {
    border: '1px solid black',
    borderRadius: '50%',
    backgroundColor: 'white',
    padding: 2,
    margin: 'auto',
    transform: 'translateX(-50%) translateY(-50%)',
    display: 'inline-block',
    color: props.place.search ? 'blue' : (props.place.needOrHave ? 'red' : 'green'),
    cursor: 'pointer',
    zIndex: 10,
    fontSize: 20,
  };

  return (
    <Fragment>
      <div style={markerStyle}><i className="fas fa-home"></i></div>
      {props.show && <InfoWindow place={props.place} openChat={props.openChat} user={props.user}/>}
    </Fragment>
  );
};

class HelpMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mapApiLoaded: false,
      mapInstance: null,
      mapApi: null,
      circle: null,
      startLoc: {},
      showMarkers: false,
      searchMarker: null,
      places: [],
      heatmapData: {
        positions: [],
        options: {
          radius: 20,
          opacity: 0.5,
        },
      }
    };

    this.fetchPlaces = this.fetchPlaces.bind(this);

    this.placeSearchMarker = this.placeSearchMarker.bind(this);
  }

  // TODO: Move circle rendering code to
  // OR: modify this component to have markers enabled/disabled

  apiHasLoaded = (map, maps) => {
    console.log("Loaded!");
    map.addListener('zoom_changed', () => {
      this.setState({showMarkers: map.getZoom() >= mapZoomLevels.showMarkers});
    });

    this.setState({
      mapApiLoaded: true,
      mapInstance: map,
      mapApi: maps,
      circle: null
    });
  };

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    let startLoc = {
      lat: parseFloat(urlParams.get('lat')),
      lng: parseFloat(urlParams.get('lng'))
    }
    //const disableMarkers = urlParams.get('nomarkers');
    if (!startLoc.lat || !startLoc.lng) {
      startLoc = this.props.defaultCenter;
    }
    //console.log("Fetching places...");
    
    this.setState({ startLoc: startLoc, showMarkers: false });
    setTimeout(this.fetchPlaces, 500);
  }

  fetchPlaces () {
    fetch('/places', {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      },
    }).then(response => {
      return response.json();
    }).then(res => {
      console.log(res);
      const users = res.users;
      let places = [];
      users.forEach(user => {
        user.places.forEach(place => {
          place.show = false;
          place.user = {
            id: user._id,
            firstname: user.firstname,
            surname: user.surname,
            email: user.email,
            phone: user.phone
          }
          places.push(place);
        });
      });
      const data = places.map(place => ({
        lat: place.lat,
        lng: place.lng,
        weight: 1,
      }));
      const heatmapData = {
        positions: data,
        options: {
          radius: 20,
          opacity: 0.5,
        },
      };
      this.setState({ places: places, heatmapData: heatmapData });
      console.log(this.state.places);
    });
  }

  // onChildClick callback can take two arguments: key and childProps
  onChildClickCallback = (key) => {
    this.setState((state) => {
      const index = state.places.findIndex(e => e._id === key);
      state.places[index].show = !state.places[index].show; // eslint-disable-line no-param-reassign
      return { places: state.places };
    });
  };

  render() {
    const {
      places, mapApiLoaded, mapInstance, mapApi, searchMarker, heatmapData
    } = this.state;

    return (
      <Fragment>
        <GoogleMap
          bootstrapURLKeys={{
              key: "AIzaSyCmpEGgQ3qZjSVAEmf0VUlPI2ypl5D0b20",
              libraries: ['visualization', 'places', 'geometry']
          }}
          defaultCenter={this.state.startLoc}
          defaultZoom={this.props.defaultZoom}
          heatmap={heatmapData}
          onChildClick={this.onChildClickCallback}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({ map, maps }) => this.apiHasLoaded(map, maps)}
        >
          {this.state.showMarkers ? places.map(place =>(
              <Marker
                openChat={this.props.openChat}
                key={place._id}
                lat={place.lat}
                lng={place.lng}
                show={place.show}
                place={place}
                user={this.props.user}
              />)
          ) : ""}
          {searchMarker &&
            <Marker
              lat={searchMarker.lat}
              lng={searchMarker.lng}
              show={searchMarker.show}
              place={searchMarker}
              />}
        </GoogleMap>
      </Fragment>
    );
  }

  search (postcode) {
    var request = {
      query: postcode,
      fields: ['name', 'geometry'],
    };
  
    var service = new this.state.mapApi.places.PlacesService(this.state.mapInstance);
    
    let searchFunc = function(results, status) {
      if (status === this.state.mapApi.places.PlacesServiceStatus.OK) {
        const loc = results[0].geometry.location;
        this.placeSearchMarker(loc);
        this.assignCircle(loc.lat(), loc.lng())
        /*for (var i = 0; i < results.length; i++) {
          createMarker(results[i]);
        }*/
        this.state.mapInstance.setCenter(loc);
        this.state.mapInstance.setZoom(mapZoomLevels.postSearch);
      }
    }

    searchFunc = searchFunc.bind(this);

    service.findPlaceFromQuery(request, searchFunc);
  }

  placeSearchMarker (location) {
    this.setState({searchMarker: {
      id: -1,
      lat: location.lat(),
      lng: location.lng(),
      show: false,
      search: true,
    }});
  }

  updateRadius (radius) {
    console.log("Updating...");
    const circle = this.state.circle;
    circle.setRadius(radius);
    this.forceUpdate();
  }

  assignCircle (lat, lng) {
    
    const map = this.state.mapInstance;
    const maps = this.state.mapApi;

    if (!this.state.circle) {
      const circle = new maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map,
        center: {lat: lat, lng: lng},
        radius: 100,
        editable: false,
        draggable: false,
        geodesic: true
      });

      maps.event.addListener(circle, 'mouseover', function () {
        circle.set('editable', true);
      });

      maps.event.addListener(circle, 'mouseout', function () {
        circle.set('editable', false);
      }); 

      let radiusFunc = function () {
        if (this.props.onCircleDrag) {
          if (this.props.onCircleDrag.current.updateRadius) {
            this.props.onCircleDrag.current.updateRadius(circle.radius);
          }
        }
      }

      radiusFunc = radiusFunc.bind(this);

      maps.event.addListener(circle, 'radius_changed', radiusFunc); 

      radiusFunc();

      circle.setMap(map);
      this.setState({circle: circle});
    } else {
      const circle = this.state.circle;
      circle.center = {lat: lat, lng: lng};
    }
  }
}

export default HelpMap;