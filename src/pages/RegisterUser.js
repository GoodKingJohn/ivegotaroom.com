import React, { Component } from 'react';
import RegisterForm from './userForms/RegisterForm';
import LoginForm from './userForms/LoginForm';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class RegisterUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            register: false
        }
        this.switchForm = this.switchForm.bind(this);
    }

    render() {

        return (
            <div className="pageContainer">
                <br/><br/>
                <div className="infoBannerHeader" style={{backgroundColor:"green", color:"black"}}>
                    <h3><div className="button" onClick={() => {this.switchForm(false)}}>Login</div> / <div className="button" onClick={() => {this.switchForm(true)}}>Register</div></h3>
                </div>
                <div className="infoBanner">
                    Registering as a User allows you to get in contact with people and register yourself for giving and getting help.<br/>
                    It takes a few minutes and makes helping a mate much easier!
                </div>
                {this.state.register ? <RegisterForm onSubmit={this.props.onSubmit} /> : <LoginForm onSubmit={this.props.onSubmit} />}
            </div>
        );
    }

    switchForm(form) {
        this.setState({register: form});
    }

}

export default RegisterUser;