import React, { Component } from 'react';
import Dropdown from '../Dropdown';
import Cookies from 'universal-cookie';
import { withRouter } from 'react-router-dom';

const cookies = new Cookies();

class HelpMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            radius: 0,
            errors: "",
            user: null
        }
        this.searchInputRef = React.createRef();
        this.radiusRef = React.createRef();

        this.search = this.search.bind(this);
        this.submit = this.submit.bind(this);
        this.handleSearchKeyDown = this.handleSearchKeyDown.bind(this);
        this.handleRadiusKeyDown = this.handleRadiusKeyDown.bind(this);
        this.handleRadiusInputChange = this.handleRadiusInputChange.bind(this);
    }

    componentDidMount() {
        fetch('/checksession', {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            headers: {
                "Content-Type": "application/json"
                // 'Content-Type': 'application/x-www-form-urlencoded',
            }
        }).then((response) => {
            return response.json();
        }).then((res) => {
            if (res.user) {
                console.log(res.user);
                this.setState({ user: res.user });
            } else {
                alert("You must be logged in to view this page!");
                this.props.history.push("/");
            }
        });
    }

    render() {

        return (
            <div className="mapBottomBar">
                <div className="infoBanner" style={{marginBottom: 0}}>
                    Find your location and drag the circle to how far you're able to give help!<br />
                    Search: <input type="text" onKeyDown={this.handleSearchKeyDown} ref={this.searchInputRef}></input> <div style={{ display: "inline-block" }} className="dropdown" onClick={this.search}>Go</div><br />
                    Help radius: <input type="text" onKeyDown={this.handleRadiusKeyDown} defaultValue={100} ref={this.radiusRef} onChange={this.handleRadiusInputChange}></input> km <p style={{ color: "red" }}>{this.state.errors}</p>
                    <div className="dropdown" style={{ display: "inline-block" }} onClick={this.submit}>Register</div>
                </div>
            </div>
        );
    }

    submit() {
        if (!this.props.map.current.state.searchMarker) {
            this.setState({ errors: this.state.errors + " No location selected!" });
            return;
        }
        if (confirm("Submit details for registering?")) {
            const formValues = cookies.get("formValues");
            formValues.lat = this.props.map.current.state.searchMarker.lat;
            formValues.lng = this.props.map.current.state.searchMarker.lng;
            formValues.radius = this.state.radius;
            formValues.user = this.state.user.id;
            fetch('/register/place', {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                headers: {
                    "Content-Type": "application/json"
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: JSON.stringify({ formValues: formValues }) // body data type must match "Content-Type" header
            }).then(response => {
                console.log(response);
            });
        }
    }

    handleSearchKeyDown(e) {
        if (e.key === 'Enter') {
            this.search();
        }
    }

    search() {
        const postcode = this.searchInputRef.current.value;
        if (this.props.map.current) {
            this.props.map.current.search(postcode);
        } else {
            console.log("No search func found");
        }
    }

    handleRadiusKeyDown(e) {
        if (e.key === 'Enter') {
            console.log("Contacting map...")
            this.props.map.current.updateRadius(parseFloat(this.radiusRef.current.value * 1000));
        }
    }

    handleRadiusInputChange() {
        const newValue = this.radiusRef.current.value;
        const match = newValue.match(/^[0-9]+\.?[0-9]*$/g);
        if (!match) {
            this.setState({ errors: "Radius must be a number!" });
        } else {
            if (this.state.errors) {
                this.setState({ errors: "" });
            }
        }
    }

    updateRadius(radius) {
        this.setState({ radius: radius });
        const radiusKm = Math.round(this.state.radius / 100) / 10;
        this.radiusRef.current.value = radiusKm;
        this.handleRadiusInputChange();
    }

}

export default HelpMap;