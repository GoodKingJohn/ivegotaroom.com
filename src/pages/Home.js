import React, { Component } from 'react';
import Dropdown from '../Dropdown';

class Home extends Component {

    constructor (props) {
        super(props);
        this.handleNeedSelect = this.handleNeedSelect.bind(this);
        this.handleHaveSelect = this.handleHaveSelect.bind(this);
    }

    render() {
        return (
            <div className="pageContainer">
                <div className="infoBanner">
                    ivegotaroom.com is here to connect people in the event of a national or other disaster.<br />
                    This is your place to find temporary accommodation, space for your animals, or feed, fodder or water for your livestock.<br /><br />
                    How can you help?<br /><br />
                    Got a spare room? Help a mate<br /><br />
                    Got some space for an animal? Help a mate<br /><br />
                    Can you offer fodder, feed or water? Go on, help a mate!<br /><br />
                    Use the map below to see who needs help and who can give it.
                </div>
                <div className="infoBannerHeader">
                    <h3>GET OR GIVE HELP NOW</h3>
                </div>
                <div className="infoBanner">
                    <table className="ddTable">
                        <tbody className="ddTBody">
                            <tr className="ddTr">
                                <th className="ddTh need">
                                    <i className="fas fa-exclamation-triangle"></i> I need...
                                </th>
                                <th className="ddTh have">
                                    <i className="fas fa-hand-holding-heart"></i> I have...
                                </th>
                            </tr>
                            <tr className="ddTr">
                                <td className="ddTd">
                                    <Dropdown options={["A room", "Help for my animals", "Feed, fodder or water"]} onSelect={this.handleNeedSelect} />
                                </td>
                                <td className="ddTd">
                                    <Dropdown options={["A room", "Space for an animal", "Feed, fodder or water"]} onSelect={this.handleHaveSelect} />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    handleNeedSelect (index, item) {
        switch (index) {
            case 0:
                this.props.history.push('/need/room');
                break;
            case 1:
                this.props.history.push('/need/animal');
                break;
            case 2:
                this.props.history.push('/need/resource');
                break;
        }
    }

    handleHaveSelect (index, item) {
        switch (index) {
            case 0:
                this.props.history.push('/have/room');
                break;
            case 1:
                this.props.history.push('/have/animal');
                break;
            case 2:
                this.props.history.push('/have/resource');
                break;
        }
    }

}

export default Home;