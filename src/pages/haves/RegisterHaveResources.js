import React, { Component } from 'react';
import Dropdown from '../../Dropdown';
import { Link } from 'react-router-dom';

class RegisterHaveAnimal extends Component {

    constructor(props) {
        super(props);
        this.subtypeRef = React.createRef();
        this.notesRef = React.createRef();
    }

    render() {

        const date = new Date();
        const today = date.toISOString().substr(0, 10);

        return (
            <div className="pageContainer">
                <br/><br/>
                <div className="infoBannerHeader" style={{backgroundColor:"green"}}>
                    <h3>Register a Have: Resources</h3>
                </div>
                <div className="infoBanner">
                    Register your Have to show others what you have and where you are located, and allow those who need your help to get it!<br/>
                    Registering takes only a couple minutes and can make a world of difference for someone.<br/>
                    Use the form below to get started.<br/>
                </div>
                <div className="infoBanner">
                    <table className="ddTable">
                        <tbody className="ddTBody">
                            <tr className="ddTr">
                                <td className="ddTd">
                                    I have:
                                </td>
                                <td className="ddTd">
                                    <Dropdown options={["Food", "Fodder", "Water", "Other"]} onSelect={this.handleNeedSelect} />
                                </td>
                            </tr>
                            <tr className="ddTr">
                                <td className="ddTd">
                                    I have this amount:
                                </td>
                                <td className="ddTd">
                                    <input type="text" />
                                </td>
                            </tr>
                            <tr className="ddTr">
                                <td className="ddTd">
                                    I am able to give for this time period:
                                </td>
                                <td className="ddTd">
                                    From <input type="date" value={today}/> to <input type="date"/>
                                </td>
                            </tr>
                            <tr className="ddTr">
                                <td className="ddTd">
                                    Additional notes:
                                </td>
                                <td className="ddTd">
                                    <textarea></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Link to="/have/map">
                        <div className="dropdown" onClick={this.handleMapClick}>Proceed to Map</div>
                    </Link>
                </div>
            </div>
        );
    }

}

export default RegisterHaveAnimal;