import React, { Component } from 'react';
import Dropdown from '../../Dropdown';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class RegisterHaveRoom extends Component {

    constructor(props) {
        super(props);
        this.state = {
            quantity: "",
            errors: ""
        }
        this.subtypeRef = React.createRef();
        this.notesRef = React.createRef();
        this.fromRef = React.createRef();
        this.toRef = React.createRef();

        this.handleQuantitySelect = this.handleQuantitySelect.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {

        const date = new Date();
        const today = date.toISOString().substr(0, 10);

        return (
            <div className="pageContainer">
                <br/><br/>
                <div className="infoBannerHeader" style={{backgroundColor:"green"}}>
                    <h3>Register a Have: Room</h3>
                </div>
                <div className="infoBanner">
                    Register your Have to show others what you have and where you are located, and allow those who need your help to get it!<br/>
                    Registering takes only a couple minutes and can make a world of difference for someone.<br/>
                    Use the form below to get started.<br/>
                </div>
                <div className="infoBanner">
                    <p style={{color:"red"}}>{this.state.errors}</p>
                    <table className="ddTable">
                        <tbody className="ddTBody">
                            <tr className="ddTr">
                                <td className="ddTd">
                                    I have space for: <p className="red">*</p>
                                </td>
                                <td className="ddTd">
                                    <Dropdown options={[1, 2, 3, 4, "5+"]} onSelect={this.handleQuantitySelect} />
                                </td>
                            </tr>
                            <tr className="ddTr">
                                <td className="ddTd">
                                    Who I can accomodate: <p className="red">*</p>
                                </td>
                                <td className="ddTd">
                                    <textarea ref={this.subtypeRef}/>
                                </td>
                            </tr>
                            <tr className="ddTr">
                                <td className="ddTd">
                                    I am able to host for this time period:
                                </td>
                                <td className="ddTd">
                                    From <p className="red">*</p> <input type="date" defaultValue={today} ref={this.fromRef}/> to <input type="date" ref={this.toRef}/>
                                </td>
                            </tr>
                            <tr className="ddTr">
                                <td className="ddTd">
                                    Additional notes:
                                </td>
                                <td className="ddTd">
                                    <textarea ref={this.notesRef}></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div className="dropdown" onClick={this.handleSubmit}>Proceed to Map</div>
                </div>
            </div>
        );
    }

    handleQuantitySelect (id, item) {
        this.setState({quantity: item});
    }

    handleSubmit() {
        if (!this.fromRef.current.value ||
            !this.state.quantity ||
            !this.subtypeRef.current.value)
        {
            this.setState({errors: "All fields marked with * are required!"});
            console.log("Required fields are null");
            return;
        } else {
            this.setState({errors: ""});
        }
        cookies.set("formValues", {
            needOrHave: false,
            type: 0,
            dateFrom: this.fromRef.current.value,
            dateTo: this.toRef.current.value,
            quantity: this.state.quantity,
            subtype: this.subtypeRef.current.value,
            notes: this.notesRef.current.value},
            {path: '/'});
        this.props.history.push("/map");
    }

}

export default RegisterHaveRoom;