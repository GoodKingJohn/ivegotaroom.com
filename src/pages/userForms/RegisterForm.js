import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class RegisterForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errors: ""
        }
        this.firstnameRef = React.createRef();
        this.surnameRef = React.createRef();
        this.passwordRef = React.createRef();
        this.confPasswordRef = React.createRef();
        this.emailRef = React.createRef();
        this.phoneRef = React.createRef();

        this.handleConfirmPasswordChange = this.handleConfirmPasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {

        return (
            <div className="infoBanner">
                <b>Register</b>
                <p style={{color:"red"}}>{this.state.errors}</p>
                <table className="ddTable">
                    <tbody className="ddTBody">
                        <tr className="ddTr">
                            <td className="ddTd">
                                Firstname <p className="red">*</p> and Surname: <p className="red">*</p>
                            </td>
                            <td className="ddTd">
                                <input type="text" ref={this.firstnameRef}/> <input type="text" ref={this.surnameRef}/>
                            </td>
                        </tr>
                        <tr className="ddTr">
                            <td className="ddTd">
                                Password: <p className="red">*</p>
                            </td>
                            <td className="ddTd">
                                <input type="password" ref={this.passwordRef}/>
                            </td>
                        </tr>
                        <tr className="ddTr">
                            <td className="ddTd">
                                Confirm password: <p className="red">*</p>
                            </td>
                            <td className="ddTd">
                                <input type="password" onChange={this.handleConfirmPasswordChange} ref={this.confPasswordRef}/>
                            </td>
                        </tr>
                        <tr className="ddTr">
                            <td className="ddTd">
                                Email: <p className="red">*</p>
                            </td>
                            <td className="ddTd">
                                <input type="text" ref={this.emailRef}/>
                            </td>
                        </tr>
                        <tr className="ddTr">
                            <td className="ddTd">
                                Phone number:
                            </td>
                            <td className="ddTd">
                                <input type="text" ref={this.phoneRef}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="dropdown" onClick={this.handleSubmit}>Register</div>
            </div>
        );
    }

    handleConfirmPasswordChange () {
        if (this.passwordRef.current.value !== this.confPasswordRef.current.value) {
            this.setState({errors: "Confirm password does not match Password!"});
        } else {
            this.setState({errors: ""});
        }
    }

    handleSubmit() {
        let errors = "";
        if (!this.firstnameRef.current.value ||
            !this.surnameRef.current.value ||
            !this.passwordRef.current.value ||
            !this.confPasswordRef.current.value ||
            !this.emailRef.current.value)
        {
            errors = "All fields marked with * are required!";
        }
        if (this.passwordRef.current.value !== this.confPasswordRef.current.value) {
            errors += " Confirm password does not match Password!";
        }
        if (!this.emailRef.current.value.match(/^[a-z,A-Z,0-9]+@([a-z,A-Z,0-9]|\.)+$/g) && this.emailRef.current.value) {
            errors += " Invalid email!";
        }
        if (!this.phoneRef.current.value.match(/^\+?[0-9]+$/g) && this.phoneRef.current.value) {
            errors += " Invalid phone number!";
        }

        if (errors) {
            this.setState({errors: errors});
            return;
        }
        this.setState({errors: ""});
        const formValues = {
            firstname: this.firstnameRef.current.value,
            surname: this.surnameRef.current.value,
            password: this.passwordRef.current.value,
            email: this.emailRef.current.value,
            phone: this.phoneRef.current.value
        }
        fetch('/register/user', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            headers: {
              "Content-Type": "application/json"
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: JSON.stringify({formValues:formValues}) // body data type must match "Content-Type" header
        }).then(response => {
            return response.json();
        }).then(res => {
            if (this.props.onSubmit) {
                console.log("Calling user check");
                this.props.onSubmit();
            } else {
                console.log("No user check found");
            }
        });
    }

}

export default withRouter(RegisterForm);