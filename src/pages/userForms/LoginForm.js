import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errors: ""
        }
        this.passwordRef = React.createRef();
        this.emailRef = React.createRef();

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {

        return (
            <div className="infoBanner">
                <b>Login</b>
                <p style={{color:"red"}}>{this.state.errors}</p>
                <table className="ddTable">
                    <tbody className="ddTBody">
                        <tr className="ddTr">
                            <td className="ddTd">
                                Email: <p className="red">*</p>
                            </td>
                            <td className="ddTd">
                                <input type="text" ref={this.emailRef}/>
                            </td>
                        </tr>
                        <tr className="ddTr">
                            <td className="ddTd">
                                Password: <p className="red">*</p>
                            </td>
                            <td className="ddTd">
                                <input type="password" ref={this.passwordRef}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="dropdown" onClick={this.handleSubmit}>Login</div>
            </div>
        );
    }

    handleSubmit() {
        
        let errors = "";
        if (!this.passwordRef.current.value ||
            !this.emailRef.current.value)
        {
            errors = "All fields marked with * are required!";
        }

        if (errors) {
            this.setState({errors: errors});
            return;
        }
        this.setState({errors: ""});
        fetch('/login', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            headers: {
              "Content-Type": "application/json"
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: JSON.stringify({
                email: this.emailRef.current.value,
                password: this.passwordRef.current.value
            }) // body data type must match "Content-Type" header
        }).then(response => {
            return response.json();
        }).then(res => {
            console.log(res);
            if (res.loggedin) {
                if (this.props.onSubmit) {
                    this.props.onSubmit();
                }
                this.props.history.push("/");
            } else {
                this.setState({errors:"Incorrect email or password!"});
            }
        });
    }

}

export default withRouter(LoginForm);