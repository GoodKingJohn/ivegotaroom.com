import React, { Component } from 'react';
import Dropdown from '../Dropdown';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import './Profile.css';
import ChatDisplay from './ChatDisplay';

const cookies = new Cookies();

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            quantity: "",
            errors: "",
            user: null
        }
        this.subtypeRef = React.createRef();
        this.notesRef = React.createRef();
        this.fromRef = React.createRef();
        this.toRef = React.createRef();

        this.handleQuantitySelect = this.handleQuantitySelect.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    checkForUser(callback) {
        console.log("Checking for user login...");
        fetch('/checksession', {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            headers: {
                "Content-Type": "application/json"
                // 'Content-Type': 'application/x-www-form-urlencoded',
            }
        }).then((response) => {
            return response.json();
        }).then((res) => {
            if (res.user) {
                console.log(res.user);
                this.setState({ user: res.user });
                callback(res.user);
            } else {
                this.setState({ user: null });
                callback(null);
            }
        });
    }

    componentDidMount () {
        this.checkForUser(user => {
            if (!user) {
                alert("You must be logged in to view this page!");
                this.props.history.push("/");
            }
        });
    }

    render() {

        const date = new Date();
        const today = date.toISOString().substr(0, 10);

        return (
            <div className="pageContainer">
                <br/><br/>
                <div className="infoBannerHeader title">
                    <div className="name">{this.state.user && <h3>{this.state.user.firstname} {this.state.user.surname}'s Profile</h3>}</div>
                </div>
                <div className="infoBanner">
                    Register your Have to show others what you have and where you are located, and allow those who need your help to get it!<br/>
                    Registering takes only a couple minutes and can make a world of difference for someone.<br/>
                    Use the form below to get started.<br/>
                </div>
                <div className="infoBanner">
                    <p style={{color:"red"}}>{this.state.errors}</p>
                    <table className="ddTable">
                        <tbody className="ddTBody">
                            <tr className="ddTr">
                                <td className="ddTd">
                                    Get pending: <p className="red">*</p>
                                </td>
                                <td className="ddTd">
                                    <ChatDisplay/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div className="dropdown" onClick={this.handleSubmit}>Proceed to Map</div>
                </div>
            </div>
        );
    }

    handleQuantitySelect (id, item) {
        this.setState({quantity: item});
    }

    handleSubmit() {
        if (!this.fromRef.current.value ||
            !this.state.quantity ||
            !this.subtypeRef.current.value)
        {
            this.setState({errors: "All fields marked with * are required!"});
            console.log("Required fields are null");
            return;
        } else {
            this.setState({errors: ""});
        }
        cookies.set("formValues", {
            needOrHave: false,
            type: 0,
            dateFrom: this.fromRef.current.value,
            dateTo: this.toRef.current.value,
            quantity: this.state.quantity,
            subtype: this.subtypeRef.current.value,
            notes: this.notesRef.current.value},
            {path: '/'});
        this.props.history.push("/map");
    }

}

export default Profile;