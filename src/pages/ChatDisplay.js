import React, { Component, Fragment } from 'react';
import socketIOClient from "socket.io-client";
import { withRouter } from "react-router-dom";
import './ChatDisplay.css';

class Chatbox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            contacts: []
        };

        this.contentRef = React.createRef();
        this.lastMsgRef = React.createRef();

        this.socket = null;
        this.minimise = this.minimise.bind(this);
        this.inputRef = React.createRef();
        this.handleInputKeyDown = this.handleInputKeyDown.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
        this.loadChat = this.loadChat.bind(this);
        this.getPending = this.getPending.bind(this);
        this.getContacts = this.getContacts.bind(this);
    }

    componentDidMount () {
        this.socket = socketIOClient('http://localhost:3001');
        /*this.socket.on("message update", data => {
            this.setState({ chat: this.state.chat.concat([{fromMe: data.from === this.props.user.id, content: data.content, timeSent: data.timeSent}])});
        });
        this.socket.on("message failed", data => {
            this.setState({ chat: this.state.chat.concat([{failed: true, fromMe: data.from === this.props.user.id, content: data.content, timeSent: data.timeSent}])});
            this.scroll();
        });
        this.socket.on("message load", data => {
            const messages = data.map((message) => ({fromMe: message.from === this.props.user.id, content: message.content, timeSent: message.timeSent}));
            const loadedAll = messages.length === 0;
            this.setState({ chat: messages.concat(this.state.chat), loadedAll: loadedAll});
        });
        this.socket.on("scroll", () => {
            this.scroll();
        });*/
        this.socket.on("pending", data => {
            console.log(data);
        });

        this.socket.on("contacts", data => {
            this.setState({contacts: data});
        });
        this.socket.emit("get contacts");
        //this.contentRef.current.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount () {
        //this.contentRef.current.removeEventListener('scroll', this.handleScroll);
    }

    updateChat () {
        this.setState({chat: []}, () => {
            const date = new Date();
            const today = date.toISOString();
            this.socket.emit("get messages", {user: this.props.user.id, other: this.props.to.id, number: 10, timeFrom: today, init: true});
        });
    }

    loadChat () {
        const timeFrom = this.state.chat[0].timeSent;
        this.socket.emit("get messages", {user: this.props.user.id, other: this.props.to.id, number: 10, timeFrom: timeFrom});
    }

    open () {
        this.setState({minimised: false});
    }

    scroll () {
        this.contentRef.current.scrollTop = this.contentRef.current.scrollHeight;
    }

    render () {
        return (
            <div className="chatDisplay">
                <table className="center">
                    <tbody className="center">
                        {this.state.contacts.map((contact, index) => (
                            <tr key={index} className="center">
                                <td className="contact">
                                    {contact}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button onClick={this.getPending}>Get pending</button>
                <button onClick={this.getContacts}>Get contacts</button>
            </div>
        )
    }  

    getPending () {
        this.socket.emit("get pending");
    }

    getContacts() {
        this.socket.emit("get contacts");
    }

    minimise () {
        if (this.props.user) {
            this.setState({minimised: !this.state.minimised});
        } else {
            //this.props.history.push("/register");
        }
    }

    handleInputKeyDown(e) {
        if (e.key === 'Enter') {
            //this.setState({chat: this.state.chat.concat([{fromMe: true, content: this.inputRef.current.value}])});
            //console.log("Message from " + this.props.user.id + " to " + this.props.to.id)
            this.socket.emit("message sent", {from: this.props.user.id, to: this.props.to.id, content: this.inputRef.current.value});
            this.inputRef.current.value = "";
            this.scroll(true);
        }
    }

    handleScroll () {
        if (this.contentRef.current.scrollTop == 0) {
            this.loadChat ();
        }
    }

}

export default Chatbox;