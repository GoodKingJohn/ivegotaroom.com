import React, { Component } from 'react';

class ScrollButton extends Component {

    constructor (props) {
        super(props);
        this.state = {hover: false};
        this.handleClick = this.handleClick.bind(this);
        this.toggleHover = this.toggleHover.bind(this);
    }

    render () {
        return (
        <div className="divBtn" onClick={this.handleClick} onMouseEnter={this.toggleHover} onMouseLeave={this.toggleHover}>
            {this.props.children}{this.state.hover ? " " + this.props.hover : ""}
        </div>
        );
    }

    toggleHover () {
        this.setState({hover: !this.state.hover});
    }

    handleClick () {
        console.log("Scrolling to " + this.props.target.current.offsetTop);
        window.scrollTo({top: this.props.target.current.offsetTop, behavior:'smooth'});
    }

}

export default ScrollButton;